import 'package:belasting/util/translation_template.dart';
import 'package:flutter/cupertino.dart';

class TranslationModel {
  static final nl = "NL";
  static final en = "EN";
  static final uk = "UK";
  String? backButtonText;
  String? loadingText;
  String? processingText;

  TranslationModel({this.backButtonText, this.loadingText, this.processingText});

  static TranslationModel? getTranslation(Locale? locale) {
    Map<String, TranslationModel> translationList;
    translationList = TranslationTemplate.translationCollection();

    if (locale != null) {
      return translationList[locale.languageCode.toUpperCase()];
    }
    return translationList[nl];
  }
}
