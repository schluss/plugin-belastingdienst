library belasting;

import 'dart:async';
import 'dart:io';

import 'package:belasting/interface/common_plugin_dao.dart';
import 'package:belasting/template.dart';
import 'package:belasting/ui/web_view_loading.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class PluginBelasting implements CommonPluginDAO {
  Logger _logger = Logger();
  final StreamController<int> _streamController = StreamController.broadcast();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'Belastingdienst',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    var locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(callBack, siteUrl, trigger, locale, pluginName: pluginName, storeCredentialCallback: storeCredentialCallback, userName: userName, password: password),
      ),
    );
  }

//Extract data from xml file
  @override
  Future<Map<String, dynamic>> getExtractedData(String filePath) async {
    // get file as string
    File file = File(filePath);
    String xmlStr = await file.readAsString();

    // parse attributes
    Map<String, dynamic> attributes = await Template.parseXML(xmlStr, _streamController);

    // delete file
    file.deleteSync(recursive: true);

    // return attributes
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
