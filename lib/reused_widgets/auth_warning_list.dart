import 'package:belasting/constants/ui_constants.dart';
import 'package:flutter/material.dart';

class AuhtWarningList extends StatelessWidget {
  AuhtWarningList(this.children);
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    var widgetList = <Widget>[];

    children.forEach((element) {
      // Add list item
      widgetList.add(element);
      // Add space between items
      widgetList.add(SizedBox(height: 15.0));
    });

    return Column(children: widgetList);
  }
}

class AuhtWarningListItem extends StatelessWidget {
  AuhtWarningListItem(this.leading, this.text, {this.child});
  final String leading;
  final String text;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 15),
          child: Text(
            leading,
            style: TextStyle(
              color: Color.fromRGBO(165, 175, 198, 1),
              fontSize: width * 0.05,
            ),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                text,
                style: TextStyle(
                  color: UIConstants.mediumGrey,
                  fontSize: width * 0.05,
                ),
              ),
              child ?? Column()
            ],
          ),
        ),
      ],
    );
  }
}
