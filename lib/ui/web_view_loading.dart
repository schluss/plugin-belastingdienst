import 'dart:async';

import 'package:belasting/constants/ui_constants.dart';
import 'package:belasting/model/translation_model.dart';
import 'package:belasting/reused_widgets/loading_indicator.dart';
import 'package:belasting/reused_widgets/mini_header_template.dart';
import 'package:belasting/ui/web_view_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class WebViewLoading extends StatefulWidget {
  final Function callBack; // function to callback main application
  final String initialUrl;
  final String trigger;
  final String? pluginName;
  final Locale locale;
  final Function(String userName, String password)? storeCredentialCallback;
  final String? userName;
  final String? password;

  WebViewLoading(this.callBack, this.initialUrl, this.trigger, this.locale, {this.pluginName, this.storeCredentialCallback, this.userName, this.password});

  @override
  _WebViewLoadingState createState() => _WebViewLoadingState();
}

class _WebViewLoadingState extends State<WebViewLoading> {
  var _url = '';
  bool _isLoading = true;
  late Timer _timer;

  _WebViewLoadingState() {
    // set timer but not work with inappbrowser widget
    _timer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  InAppWebViewController? _appWebViewController; // caontain webview controller object

  void _afterInAppWebCreated(InAppWebViewController webViewController) {
    _appWebViewController = webViewController;
  }

  void _urlChanged(url) {
    setState(() {
      _url = url; // set url change
    });
  }

  void _callBackMethod(String status, BuildContext context) {
    widget.callBack(widget.pluginName,status, context); //callback to main application
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.initialUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var loadingText = TranslationModel.getTranslation(widget.locale)!.loadingText;

    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: !_isLoading
                  ? WebViewBody(_afterInAppWebCreated, _url, _urlChanged, _callBackMethod, widget.trigger, widget.locale, storeCredentialCallback: widget.storeCredentialCallback, userName: widget.userName, password: widget.password)
                  : Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25,
                      ),
                      child: PageLoadingView(
                        widget.locale,
                        text: _isLoading ? loadingText : TranslationModel.getTranslation(widget.locale)!.processingText,
                      ),
                    ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top,
                ),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  title: _url,
                  locale: widget.locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
