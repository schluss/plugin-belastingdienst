import 'dart:async';

import 'package:belasting/model/translation_model.dart';
import 'package:belasting/reused_widgets/wait_overlay.dart';
import 'package:belasting/ui/auth_sms_warning.dart';
import 'package:belasting/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';

Logger _logger = Logger();
Locale? _locale;
late String _trigger;
late bool _isTestEnvironment;
String? downloadedXmlPath;

class WebViewBody extends StatefulWidget {
  /// Initial url to open first.
  final String initialUrl;

  /// This uses to call after webview created.
  final Function webViewCreatedCallback;

  /// This uses to call after webview is created.
  final Function urlChangedCallback;

  /// Locale for detects the region.
  final Locale locale;

  /// This uses to trigger download process.
  final String trigger;

  /// Main app callback function.
  final Function mainAppCallBack;

  /// Username for the Belastingdienst - REAL.
  final String? userName;

  /// Password for the Belastingdienst - REAL.
  final String? password;

  /// Stores credential callback function.
  final Function(String userName, String password)? storeCredentialCallback;

  WebViewBody(
    this.webViewCreatedCallback,
    this.initialUrl,
    this.urlChangedCallback,
    this.mainAppCallBack,
    this.trigger,
    this.locale, {
    this.storeCredentialCallback,
    this.userName,
    this.password,
  }) {
    _locale = locale;

    _trigger = trigger;

    /// This checks whether use the Belastingdienst - FAKE for testing or not.
    _isTestEnvironment = initialUrl.contains('services.schluss.app');
  }

  @override
  _WebViewBodyState createState() => _WebViewBodyState();
}

class _WebViewBodyState extends State<WebViewBody> {
  /// This uses to show or hide the processing screen.
  bool _isShown = false;

  @override
  void initState() {
    super.initState();
  }

  /// Shows the processing screen.
  void showWaiter(String? text) async {
    if (!_isShown) {
      WaitOverlay.show(context, _locale, text: text);
      _isShown = true;
    }
  }

  /// Hides the processing screen.
  void hideWaiter() {
    if (_isShown) {
      WaitOverlay.hide();
      _isShown = false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _cookieManager = CookieManager.instance();

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      height: height * 0.9,
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InAppWebView(
              initialUrlRequest: URLRequest(url: Uri.parse(widget.initialUrl)),
              initialOptions: InAppWebViewGroupOptions(
                android: AndroidInAppWebViewOptions(useHybridComposition: true),
                crossPlatform: InAppWebViewOptions(
                  useOnDownloadStart: true,
                  useShouldOverrideUrlLoading: false,
                  cacheEnabled: false,
                  clearCache: true,
                ),
              ),
              onWebViewCreated: (InAppWebViewController controller) async {
                showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);

                /// Calls given function after webview created
                widget.webViewCreatedCallback(controller);
              },
              onLoadStart: (controller, url) async {
                showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);
              },
              onDownloadStart: (InAppWebViewController controller, url) async {
                var location = url.toString();
                // If triggers the download (ANDROID SPECIFIC).
                if (location.contains(_trigger)) {
                  downloadedXmlPath = await downloadTrigger(controller, location, _cookieManager);

                  // If it is the test environment.
                  if (_isTestEnvironment) {
                    /// Redirects to the fake logout page.
                    await controller.loadUrl(
                      urlRequest: URLRequest(
                        url: Uri.parse(
                          'https://services.schluss.app/belastingdienst/test/uitgelogd.html',
                        ),
                      ),
                    );
                  } else {
                    /// Redirects to the logout page.
                    await controller.loadUrl(
                      urlRequest: URLRequest(
                        url: Uri.parse(
                          'https://mijn.belastingdienst.nl/VIADownload2021/uitloggen.html',
                        ),
                      ),
                    );
                  }
                }
              },
              onLoadStop: (InAppWebViewController controller, url) async {
                var location = url.toString();

                // If it is the test environment.
                if (_isTestEnvironment) {
                  /// 1. Initial page: show fake DigiD login form.
                  if (location == widget.initialUrl) {
                    hideWaiter();
                  }

                  /// 2. fake DigiD SMS verification.
                  if (location.contains('digid-sms.html')) {
                    hideWaiter();
                  }

                  if (location.contains('belastingdienst.html')) {
                    await controller.evaluateJavascript(source: '''(function(){
                      document.querySelector("#MainContent > div.panel > a ").click();
                    })();''');
                  }

                  // If triggers the download (IOS SPECIFIC).
                  if (location.contains(_trigger)) {
                    downloadedXmlPath = await downloadTrigger(controller, location, _cookieManager);

                    /// Redirects to the logout page.
                    await controller.loadUrl(
                      urlRequest: URLRequest(
                        url: Uri.parse(
                          'https://services.schluss.app/belastingdienst/test/uitgelogd.html',
                        ),
                      ),
                    );
                  }

                  /// 6. After logout: we can assume all steps are done successfully, return to mainApp.
                  if (location.contains('uitgelogd.html')) {
                    await controller.clearCache();

                    hideWaiter();

                    widget.mainAppCallBack(downloadedXmlPath, context);
                  }
                } else {
                  /// PRODUCTION SITE.
                  /// 1. initial page: automatically make 'self' selection and continue to login entry page.
                  if (location == widget.initialUrl) {
                    await controller.evaluateJavascript(source: '''(function(){
                      document.getElementById("radioA1").checked = true; 
                      document.querySelector("#submit").click();
                    })();''');

                    hideWaiter();
                  }

                  /// 2. Generic login page: automatically select digid login method
                  if (location.contains('mijn.belastingdienst.nl/GTService/#/inloggen')) {
                    hideWaiter();

                    // Clicking on the DigiD login button needs a delay of at least 500ms, otherwise the page is not loaded fully yet
                    await controller.evaluateJavascript(source: '''window.setTimeout(function(){ 
                      document.getElementById('a_digid').click();
                     }, 500);''');
                  }

                  /// 3a. At login with DigiD: when user input it needed.

                  // If asks for username and password.
                  if (location.contains('digid.nl/inloggen')) {
                    /// Check if sms auth is enabled, if not: show error to the user.
                    bool smsAuthDisabled = await (controller.evaluateJavascript(source: '''
                      (function(){
                        try {
                        let el = document.querySelector("#main_content > div.block-with-icon--error > p");
                        if (el) {
                          console.log('innerText sms check is: ' + el.innerText);
                          return el.innerText.includes('sms');
                        }
                        return false;
                        }
                        catch {
                          return false;
                        }
                      })();'''));

                    if (smsAuthDisabled) {
                      hideWaiter();
                      Navigator.pop(context);
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AuthSMSWarning(widget.locale),
                        ),
                      );
                      return;
                    }

                    /// Shows thr login form for user to enter credentials.
                    hideWaiter();
                  }

                  /// 3b. When user needs enter sms verification code.
                  if (location == 'https://digid.nl/sms_controleren' || location == 'https://digid.nl/bevestig_telefoonnummer') {
                    hideWaiter();
                  }

                  /// 4. After login: automatically start the download by clicking the download button.
                  /// The download is then cached at 'onDownloadStart'
                  if (location == 'https://mijn.belastingdienst.nl/VIADownload2021/gegevensDownloaden.html') {
                    await controller.evaluateJavascript(source: '''(function(){
                      document.querySelector("#MainContent > div.panel > form > a").click()
                    })();''');
                  }

                  /// 5. [IOS SPECIFIC]: Trigger the download.
                  if (location.contains(_trigger)) {
                    downloadedXmlPath = await downloadTrigger(
                      controller,
                      location,
                      _cookieManager,
                    );

                    /// Redirects to the logout page.
                    await controller.loadUrl(
                      urlRequest: URLRequest(
                        url: Uri.parse('https://mijn.belastingdienst.nl/VIADownload2021/uitloggen.html'),
                      ),
                    );
                  }

                  /// 6. After logout: we can assume all steps are done successfully, return to mainApp.
                  if (location.contains(
                    'https://mijn.belastingdienst.nl/VIADownload2021/uitgelogd.html',
                  )) {
                    await controller.clearCache();

                    hideWaiter();

                    widget.mainAppCallBack(downloadedXmlPath, context);
                  }
                }
              },
            ),
          )
        ],
      ),
    );
  }
}

/// Downloads and stores xml file.
Future<String> downloadTrigger(
  InAppWebViewController controller,
  String url,
  CookieManager _cookieManager,
) async {
  List<Cookie> cookies;
  Util _util;
  String downloadValue;

  cookies = await _cookieManager.getCookies(url: Uri.parse(url));

  _logger.i('Plugin Belastingdienst - Start download xml file');

  _util = Util();
  downloadValue = await _util.downloadAndSaveFile(url, cookies);

  return downloadValue;
}
