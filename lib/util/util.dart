import 'dart:io';

import 'package:flutter_inappwebview/flutter_inappwebview.dart' as in_app;
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:requests/requests.dart';

class Util {
  Logger _logger = Logger();

  final String _fileName = 'belasting.xml';

  // File downloading and saving:
  Future<String> downloadAndSaveFile(String downloadUrl, List<in_app.Cookie> cookies) async {
    // Make a Hashmap and fill it with all needed cookies
    Map<String, String> cookiesMap = {};

    cookies.forEach((element) {
      if (element != null) cookiesMap[element.name] = element.value;
    });

    cookiesMap['testCookie'] = '/VIADownload2020';

    // Prepare request
    String hostname;
    hostname = Requests.getHostname(downloadUrl);
    await Requests.setStoredCookies(hostname, cookiesMap);

    // Do the request and get the response
    Response response;
    response = await Requests.get(downloadUrl);

    String body = response.content();

    // Store result as a file
    String _savePath;
    _savePath = await getSavePath(_fileName); // Getting file saving location.

    File f = File(_savePath);
    await f.writeAsString(body);

    return _savePath;
  }

  // Geting downloaded file saving path:
  Future<String> getSavePath(String fileName) async {
    Directory _dir;
    String _savePath;

    _dir = await getApplicationDocumentsDirectory(); // Getting downloaded files saving loaction.
    _savePath = "${_dir.path}/$fileName"; // Setting downloaded file saving loaction.

    return _savePath; // Returning downloaded file saving path.
  }
}
