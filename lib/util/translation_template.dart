import 'package:belasting/model/translation_model.dart';

class TranslationTemplate {
  static Map<String, TranslationModel> translationCollection() => {
    'EN': TranslationModel(backButtonText: 'Back', loadingText: 'You are now going to the website of Belastingdienst to login', processingText: ' Processing'),
    'UK': TranslationModel(backButtonText: 'Back', loadingText: 'You are now going to the website of Belastingdienst to login', processingText: ' Processing'),
    'NL': TranslationModel(backButtonText: 'Terug', loadingText: 'Je gaat nu naar de website van Belastingdienst om in te loggen', processingText: 'Verwerken'),
  };
}
